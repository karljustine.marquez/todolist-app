<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo List</title>
    <link href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@300&family=Bebas+Neue&family=Paytone+One&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />   
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
    <style>
    h1 {
        font-family: 'Paytone One', sans-serif;
    }

    h5 {
        font-family: 'Barlow Condensed', sans-serif;
    }

    li {
        font-family: 'Bebas Neue', cursive;
        font-size: 25px;
    }
    </style>
</head>
<body class="bg-warning">
    <div class="container w-50 mt-5">
    <div class="card shadow-sm">
        <div class="card-body">
            <h1 class="text-center">TO-DO LIST</h1>
            <h5 class="text-center">Manage your tasks and boost your productivity.</h5>
            <br><br>
            <form action="{{ route('store') }}" method="POST" autocomplete="off">
                @csrf
                <div class="input-group border rounded border-info border-4">
                    <input type="text" name="content" class="form-control" placeholder="Add your new todo">
                    <button type="submit" class="btn btn-dark btn-sm px-4"><i class="fas fa-plus"></i></button>
                </div>
            </form>
            @if (count($todolists))
            <ul class="list-group list-group-flush mt-3">
                @foreach ($todolists as $todolist)
                    <li class="list-group-item">
                        <form action="{{ route('destroy', $todolist->id) }}" method="POST">
                            {{ $todolist->content }}
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-link btn-sm float-end"><i class="fas fa-trash"></i></button>
                        </form>
                    </li>
                @endforeach
            </ul>
            @else
            <p class="text-center mt-3">No Tasks!</p>
            @endif    
        </div>
        @if (count($todolists))
            <div class="card-footer">
                You have {{ count($todolists) }} pending tasks.
            </div>
        @endif
    </div>
    </div>
    <br>
    <div>
        <img src="{{ URL::to('images/list.png') }}" width="150" class="mx-auto d-block" />
    </div>
</body>
</html>